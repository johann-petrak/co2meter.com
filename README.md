# CO2Meter.com USB Probe Gas Meter

This is a demonstration Python program for
the gas (CO2) sensor from CO2Meter.com

There are two parts: `sense.py` and `web.py`.

`sense.py` is a long-running process that
collects data from the sensor and formats it on `stdout`.

`web.py` is a web server that shows today's SVG plot,
using the contents of the file `co2.csv`.

# Installation

Python 3 is required.

## sense.py installation

In a virtualenv or a conda env, install the requirements
(for `sense.py`):

    pip install -r requirements.txt

The files and permissions are somewhat Linux specific.

`sense.py` reads data from the sensor via the USB serial port;
it needs access to the serial port
(this is currently hardwired to the port `/dev/ttyUSB0`).
Running it as root using `sudo` won't work because it won't find
your conda environment.

Either add yourself to the `dialout` group:
`sudo adduser $USER dialout`; and then reboot.

Or `chown` the tty file (every time you insert the USB cable):
`chown $USER /dev/ttyUSB0`.

Run the logger:

    python sense.py | tee -a co2.csv

## web.py installation

`pip install pandas` or equivalent.

    python web.py

Listens on 8550 by default.
