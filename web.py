#!/usr/bin/env python

# A web server that serves SVG plots of the
# time series data from the file co2.csv
#
# co2.csv
# The input file is of the form:
#
"""
2019-05-30T10:36:41.721099,482
2019-05-30T10:36:43.752226,484
2019-05-30T10:36:45.782865,486
2019-05-30T10:36:47.813809,488
"""
#
# Requires Python 3 and pandas.  Run with:
# python3 web.py


import csv
import glob
import fileinput
import http.server
import io
import itertools

# https://matplotlib.org/
import matplotlib.pyplot

# https://pypi.org/project/pandas/
import pandas

# Suggested by Will Furnass, 2019-05-31
matplotlib.pyplot.style.use("ggplot")


class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "hello":
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(b"Hello, world!\n")
            return
        if self.path == "/co2.svg":
            return self.view_svg()
        return self.view_dash()

    def view_svg(self):
        self.send_response(200)
        self.send_header("Content-type", "image/svg+xml")
        self.end_headers()

        all_csv_files = fileinput.input(glob.glob("*.csv"))
        rows = sorted(csv.reader(all_csv_files))
        rows = [(d, int(v)) for d, v in rows]
        days = [list(rs) for day, rs in itertools.groupby(rows, by_day)]

        today = days[-1]
        plot = pandas_series(today).plot(title="CO₂")
        plot.set_ylabel("ppm")
        plot.set_xlabel("Date and Time")
        fig = plot.get_figure()

        buffer = io.BytesIO()
        fig.savefig(buffer, format="svg")

        self.wfile.write(buffer.getbuffer())

    def view_dash(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(
            """
<html>
<meta charset="utf-8" http-equiv="refresh" content="55" />
<title>CO₂ level</title>
<img src="co2.svg" />
</html>
""".encode(
                "utf-8"
            )
        )
        return


def pandas_series(rows):
    """
    Convert list of rows to a pandas.Series.
    """

    timestamps = [pandas.to_datetime(d) for d, _ in rows]
    series = pandas.Series([v for _, v in rows], index=timestamps)
    return series


def by_day(row):
    """
    Return the DAY part of a row from the CSV file
    (to be used by itertools.groupby)
    """
    return row[0][:10]


address = ("localhost", 8550)
print("About to listen on: http://{}:{}/".format(*address))
server = http.server.HTTPServer(address, Handler)
server.serve_forever()
